# JSP及简单案例

## JSP的集成

### pom依赖

```xml
 <!--        jstl - jsp标准标签库-->
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>jstl</artifactId>
        </dependency>
 <!--        jsp     -->
    	<dependency>
            <groupId>org.apache.tomcat.embed</groupId>
            <artifactId>tomcat-embed-jasper</artifactId>
        </dependency>
```

### jsp资源路径

1. 在main目录下,与java和resource同级目录下创建webapp

2. webapp目录下创建WEB-INF文件夹,jsp文件夹

3. 最好按照jsp的目录结构设计

   ```
   
   │  └── webapp
     ``│    ├── index.html
     ``│    ├── META-INF
     ``│    │  ├── context.xml
     ``│    │  └── MANIFEST.MF
     ``│    ├── resources
     ``│    │  └── css
     ``│    │    └── screen.css
     ``│    └── WEB-INF
     ``│    │  └── jsp
     ``│    │     |└── index.jsp
   ```

4. 添加到web model中

   file>Project Structure>Modules>

   <img src="img/jsp1.png" style="zoom:50%;" />



### yaml配置

```yml
spring:
  mvc:
    view:
      suffix: .jsp   #默认的文件后缀
      prefix: /WEB-INF/jsp/ #默认的资源前缀
```

### 尝试以第一个jsp

```java
    @GetMapping("/login")
    public String getPage(){
        return "welcome";
    }
```



## JSP

### 什么是jsp

servlet就是一个能处理HTTP请求，发送HTTP响应的小程序，而发送响应无非就是获取`PrintWriter`，然后输出HTML：

```
PrintWriter pw = resp.getWriter();
pw.write("<html>");
pw.write("<body>");
pw.write("<h1>Welcome, " + name + "!</h1>");
pw.write("</body>");
pw.write("</html>");
pw.flush();
```

只不过，用PrintWriter输出HTML比较痛苦，因为不但要正确编写HTML，还需要插入各种变量。如果想在Servlet中输出一个类似新浪首页的HTML，写对HTML基本上不太可能。

那有没有更简单的输出HTML的办法？

#####            有！

我们可以使用JSP。

JSP是Java Server Pages的缩写，它的文件必须放到`/src/main/webapp`下，文件名必须以`.jsp`结尾，整个文件与HTML并无太大区别，但需要插入变量，或者动态输出的地方，使用特殊指令`<% ... %>`。





### jsp标签库

使用forEach标签时需要在JSP页面中引入JSTL标签库

```jsp
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
```

foreach语法

```jsp
<c:forEach items="接收集合对象" var="迭代参数名称" varStatus="迭代状态，可访问迭代自身信息">
```

