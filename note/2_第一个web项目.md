# 一、SpringBoot简介

## 1.1  原有Spring优缺点分析

### 1.1.1 Spring的优点分析

Spring是Java企业版（Java Enterprise Edition，JEE，也称J2EE）的轻量级代替品。无需开发重量级的Enterprise JavaBean（EJB），Spring为企业级Java开发提供了一种相对简单的方法，通过依赖注入和面向切面编程，用简单的Java对象（Plain Old Java Object，POJO）实现了EJB的功能。

### 1.1.2 Spring的缺点分析

虽然Spring的组件代码是轻量级的，但它的配置却是重量级的。一开始，Spring用XML配置，而且是很多XML配置。Spring 2.5引入了基于注解的组件扫描，这消除了大量针对应用程序自身组件的显式XML配置。Spring 3.0引入了基于Java的配置，这是一种类型安全的可重构配置方式，可以代替XML。

所有这些配置都代表了开发时的损耗。因为在思考Spring特性配置和解决业务问题之间需要进行思维切换，所以编写配置挤占了编写应用程序逻辑的时间。和所有框架一样，Spring实用，但与此同时它要求的回报也不少。

除此之外，项目的依赖管理也是一件耗时耗力的事情。在环境搭建时，需要分析要导入哪些库的坐标，而且还需要分析导入与之有依赖关系的其他库的坐标，一旦选错了依赖的版本，随之而来的不兼容问题就会严重阻碍项目的开发进度。

## 1.2 SpringBoot的概述

### 1.2.1 SpringBoot解决上述Spring的缺点

SpringBoot对上述Spring的缺点进行的改善和优化，基于**约定优于配置**的思想，可以让开发人员不必在配置与逻辑业务之间进行思维的切换，全身心的投入到逻辑业务的代码编写中，从而大大提高了开发的效率，一定程度上缩短了项目周期。

### 1.2.2 SpringBoot的特点

- 为基于Spring的开发提供更快的入门体验
- 开箱即用，没有代码生成，也无需XML配置。同时也可以修改默认值来满足特定的需求
- 提供了一些大型项目中常见的非功能性特性，如嵌入式服务器、安全、指标，健康检测、外部配置等
- SpringBoot不是对Spring功能上的增强，而是提供了一种快速使用Spring的方式

### 1.2.3 SpringBoot的核心功能

- 起步依赖

  起步依赖本质上是一个Maven项目对象模型（Project Object Model，POM），定义了对其他库的传递依赖，这些东西加在一起即支持某项功能。

  简单的说，起步依赖就是将具备某种功能的坐标打包到一起，并提供一些默认的功能。

- 自动配置

  Spring Boot的自动配置是一个运行时（更准确地说，是应用程序启动时）的过程，考虑了众多因素，才决定Spring配置应该用哪个，不该用哪个。该过程是Spring自动完成的。




​	注意：起步依赖和自动配置的原理剖析会在第三章《SpringBoot原理分析》进行详细讲解

# 二、SpringBoot快速入门

## 2.1 代码实现

### 2.1.1 创建SpringBoot工程

使用idea工具创建一个maven工程，该工程为普通的java工程即可

<img src="/img/1.png" style="zoom:67%;" />

<img src="img/Springboot2.png" style="zoom:50%;" />

![](/img/s3.png)

![](/img/s4.png)

![](/img/s5.png)

### 2.1.3 编写SpringBoot引导类

要通过SpringBoot提供的引导类起步SpringBoot才可以进行访问

```java
@SpringBootApplication
public class MySpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(MySpringBootApplication.class);
    }

}
```

### 2.1.4 编写Controller

在引导类MySpringBootApplication同级包或者子级包中创建QuickStartController

```java
@Controller
public class QuickStartController {
    
    @RequestMapping("/quick")
    @ResponseBody
    public String quick(){
        return "springboot 访问成功!";
    }
    
}
```

### 2.1.5 测试

执行SpringBoot起步类的主方法，控制台打印日志如下：

```
.   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v2.0.1.RELEASE)

2018-05-08 14:29:59.714  INFO 5672 --- [           main] com.itheima.MySpringBootApplication      : Starting MySpringBootApplication on DESKTOP-RRUNFUH with PID 5672 (C:\Users\muzimoo\IdeaProjects\IdeaTest\springboot_quick\target\classes started by muzimoo in C:\Users\muzimoo\IdeaProjects\IdeaTest)
... ... ...
o.s.w.s.handler.SimpleUrlHandlerMapping  : Mapped URL path [/**] onto handler of type [class org.springframework.web.servlet.resource.ResourceHttpRequestHandler]
2018-05-08 14:30:03.126  INFO 5672 --- [           main] o.s.j.e.a.AnnotationMBeanExporter        : Registering beans for JMX exposure on startup
2018-05-08 14:30:03.196  INFO 5672 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
2018-05-08 14:30:03.206  INFO 5672 --- [           main] com.itheima.MySpringBootApplication      : Started MySpringBootApplication in 4.252 seconds (JVM running for 5.583)
```

通过日志发现，Tomcat started on port(s): 8080 (http) with context path ''

tomcat已经起步，端口监听8080，web应用的虚拟工程名称为空

打开浏览器访问url地址为：http://localhost:8080/quick

![](../../工作/other/springboot_课件/day01/day01/笔记/img/5.png)

## devtools安装(热部署)

#### 方式一：无任何配置时，手动触发重启更新（Ctrl+F9）

<img src="img/dev1.png" style="zoom:50%;" />


（也可以用mvn compile编译触发重启更新）

#### 方式二：IDEA需开启运行时编译，自动重启更新

设置1：

File->Setting->Build,Execution,Deployment->Compile

勾选：Make project automatically



设置2：

快捷键：ctrl+alt+shift+/

选择：Registry

![](img/dev3.png)

勾选：compiler.automake.allow.when.app.running

新版本的IDEA可以在File->setting->Advanced Setttings里面的第一个设置：

![](img/dev4.png)



application.yml配置
登录后复制 
spring:
  devtools:
    restart:
      enabled: true  #设置开启热部署
      additional-paths: src/main/java #重启目录
      exclude: WEB-INF/**
  thymeleaf:





## 3. Lombok 安装

使 IntelliJ IDEA 支持 Lombok 方式如下：

- 

  **Intellij 设置支持注解处理**

  - 

    点击 File > Settings > Build > Annotation Processors

  - 

    勾选 Enable annotation processing
    
    ![/img/lom1.png](img/lom1.png)

- 

  **安装插件**

  - 

    点击 Settings > Plugins > Browse repositories

  - 

    查找 Lombok Plugin 并进行安装

  - 

    重启 IntelliJ IDEA

- 

  **将 lombok 添加到 pom 文件**



~~~yml
<dependency>

    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <version>1.16.8</version>

</dependency>
~~~

Lombok 提供注解 API 来修饰指定的类：

### 3.1. @Getter and @Setter

[@Getter and @Setter](http://jnb.ociweb.com/jnb/jnbJan2010.html#gettersetter) Lombok 代码：

@Getter @Setter private boolean employed = true;

@Setter(AccessLevel.PROTECTED) private String name;



等价于 Java 源码：
```java

private boolean employed = true;

private String name;



public boolean isEmployed() {

​    return employed;

}



public void setEmployed(final boolean employed) {

​    this.employed = employed;

}



protected void setName(final String name) {

​    this.name = name;

}
```


### 3.2. @NonNull

[@NonNull](http://jnb.ociweb.com/jnb/jnbJan2010.html#nonnull) Lombok 代码：

@Getter @Setter @NonNull

private List<Person> members;



等价于 Java 源码：

```java
@NonNull

private List<Person> members;



public Family(@NonNull final List<Person> members) {

​    if (members == null) throw new java.lang.NullPointerException("members");

​    this.members = members;

}



@NonNull

public List<Person> getMembers() {

​    return members;

}



public void setMembers(@NonNull final List<Person> members) {

​    if (members == null) throw new java.lang.NullPointerException("members");

​    this.members = members;

}
```





#### 01.@Data

1. @Data 是一个很方便的注解，它和@ToString、 @EqualAndHashCode、@Getter/@Setter、和@RequiredArgsConstructor 绑定在一起。换句话说，@Data 生成通常与简单的POJO(Plain Old Java Objects) 和 bean 相关联的所有样板代码，例如：获取所有的属性，设置所有不可继承的属性，适应toString、equals 和 hashcode 的实现，通过构造方法初始化所有final 的属性，以及所有没有使用@NonNull标记的初始化程序的非final字段，以确保该字段永远不为null。
2. @Data 就像在类上隐含使用 `@toString 、 @EqualAndHashCode、 @Getter、 @Setter 和 @RequiredArgsConstructor` 注释一样。`@Data = @Getter + @Setter + @ToString + @EqualsAndHashCode + @RequiredArgsConstructor`。但是，@Data 无法设置这些注解的参数，例如callSuper、includeFieldNames 和 exclude， 如果您需要为这些参数中的任何一个设置非默认值，只需显式添加这些注释;
3. 生成的所有getters/setters 默认都是public 的，为了覆盖访问级别，请使用显式的@Setter \ @Getter批注对字段或类进行注释。你可以使用这个注释(通过与 AccessLevel.NONE结合)来禁止使用 getter或setter。
4. 所有使用 transient 标记的字段都不会视为 hashcode 和 equals。将完全跳过所有静态字段（不考虑任何生成的方法，并且不会为它们创建setter / getter）。
5. 如果类已经包含与通常生成的任何方法具有相同名称和参数计数的方法，则不会生成该方法，也不会发出警告或错误。 例如：如果你使用 equals 标记了一个方法，那么不会再生成 equals 方法，即使从技术上讲，由于具有不同的参数类型，它可能是完全不同的方法。同样的规则适用于构造函数（任何显式构造函数都会阻止 @Data 生成一个），以及toString，equals和所有getter和setter。
6. 您可以使用@ lombok.experimental.Tolerate 标记任何构造函数或方法，以将它们隐藏在 lombok 中 例如：

```kotlin
kotlin复制代码import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import lombok.ToString;

@Data
public class DataExample {

    private final String name;

    @Setter(AccessLevel.PACKAGE)
    private int age;

    private double score;

    private String[] tags;

    @ToString(includeFieldNames = true)
    @Data(staticConstructor = "of")
    public static class Exercise<T> {
        private final String name;
        private final T value;
    }

}
 
=========就相当于是，不用 lombok 的如下示例：=========
 
import java.util.Arrays;

public class DataExample {
  private final String name;
  private int age;
  private double score;
  private String[] tags;

  public DataExample(String name) {
    this.name = name;
  }

  public String getName() {
    return this.name;
  }

  void setAge(int age) {
    this.age = age;
  }

  public int getAge() {
    return this.age;
  }

  public void setScore(double score) {
    this.score = score;
  }

  public double getScore() {
    return this.score;
  }

  public String[] getTags() {
    return this.tags;
  }

  public void setTags(String[] tags) {
    this.tags = tags;
  }

  @Override 
  public String toString() {
    return "DataExample(" + this.getName() + ", " + this.getAge() + ", " + this.getScore() + ", " + Arrays.deepToString(this.getTags()) + ")";
  }

  protected boolean canEqual(Object other) {
    return other instanceof DataExample;
  }

  @Override 
  public boolean equals(Object o) {
    if (o == this) return true;
    if (!(o instanceof DataExample)) return false;
    DataExample other = (DataExample) o;
    if (!other.canEqual((Object)this)) return false;
    if (this.getName() == null ? other.getName() != null : !this.getName().equals(other.getName())) return false;
    if (this.getAge() != other.getAge()) return false;
    if (Double.compare(this.getScore(), other.getScore()) != 0) return false;
    if (!Arrays.deepEquals(this.getTags(), other.getTags())) return false;
    return true;
  }

  @Override 
  public int hashCode() {
    final int PRIME = 59;
    int result = 1;
    final long temp1 = Double.doubleToLongBits(this.getScore());
    result = (result*PRIME) + (this.getName() == null ? 43 : this.getName().hashCode());
    result = (result*PRIME) + this.getAge();
    result = (result*PRIME) + (int)(temp1 ^ (temp1 >>> 32));
    result = (result*PRIME) + Arrays.deepHashCode(this.getTags());
    return result;
  }

  public static class Exercise<T> {
    private final String name;
    private final T value;

    private Exercise(String name, T value) {
      this.name = name;
      this.value = value;
    }

    public static <T> Exercise<T> of(String name, T value) {
      return new Exercise<T>(name, value);
    }

    public String getName() {
      return this.name;
    }

    public T getValue() {
      return this.value;
    }

    @Override 
    public String toString() {
      return "Exercise(name=" + this.getName() + ", value=" + this.getValue() + ")";
    }

    protected boolean canEqual(Object other) {
      return other instanceof Exercise;
    }

    @Override 
    public boolean equals(Object o) {
      if (o == this) return true;
      if (!(o instanceof Exercise)) return false;
      Exercise<?> other = (Exercise<?>) o;
      if (!other.canEqual((Object)this)) return false;
      if (this.getName() == null ? other.getValue() != null : !this.getName().equals(other.getName())) return false;
      if (this.getValue() == null ? other.getValue() != null : !this.getValue().equals(other.getValue())) return false;
      return true;
    }

    @Override 
    public int hashCode() {
      final int PRIME = 59;
      int result = 1;
      result = (result*PRIME) + (this.getName() == null ? 43 : this.getName().hashCode());
      result = (result*PRIME) + (this.getValue() == null ? 43 : this.getValue().hashCode());
      return result;
    }
  }
}
```



## 2.2 快速入门解析

### 2.2.2 SpringBoot代码解析

- @SpringBootApplication：标注SpringBoot的启动类，该注解具备多种功能（后面详细剖析）
- SpringApplication.run(MySpringBootApplication.class) 代表运行SpringBoot的启动类，参数为SpringBoot启动类的字节码对象

### 2.2.3 SpringBoot工程热部署

我们在开发中反复修改类、页面等资源，每次修改后都是需要重新启动才生效，这样每次启动都很麻烦，浪费了大量的时间，我们可以在修改代码后不重启就能生效，在 pom.xml 中添加如下配置就可以实现这样的功能，我们称之为热部署。

```xml
<!--热部署配置-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-devtools</artifactId>
</dependency>
```

注意：IDEA进行SpringBoot热部署失败原因

出现这种情况，并不是热部署配置问题，其根本原因是因为Intellij IEDA默认情况下不会自动编译，需要对IDEA进行自动编译的设置，如下：

![](../../工作/other/springboot_课件/day01/day01/笔记/img/19.png)

然后 Shift+Ctrl+Alt+/，选择Registry

![](../../工作/other/springboot_课件/day01/day01/笔记/img\20.png)

# 三、SpringBoot的配置文件

## 3.1 SpringBoot配置文件类型

### 3.1.1 SpringBoot配置文件类型和作用

SpringBoot是基于约定的，所以很多配置都有默认值，但如果想使用自己的配置替换默认配置的话，就可以使用application.properties或者application.yml（application.yaml）进行配置。

SpringBoot默认会从Resources目录下加载application.properties或application.yml（application.yaml）文件

其中，application.properties文件是键值对类型的文件，之前一直在使用，所以此处不在对properties文件的格式进行阐述。除了properties文件外，SpringBoot还可以使用yml文件进行配置，下面对yml文件进行讲解。

### 3.1.2 application.yml配置文件

#### 3.1.2.1 yml配置文件简介

YML文件格式是YAML (YAML Aint Markup Language)编写的文件格式，YAML是一种直观的能够被电脑识别的的数据数据序列化格式，并且容易被人类阅读，容易和脚本语言交互的，可以被支持YAML库的不同的编程语言程序导入，比如： C/C++, Ruby, Python, Java, Perl, C#, PHP等。YML文件是以数据为核心的，比传统的xml方式更加简洁。

YML文件的扩展名可以使用.yml或者.yaml。

##### 配置普通数据

- 语法： key: value

- 示例代码：

- ```yaml
  name: haohao
  ```

- 注意：value之前有一个空格