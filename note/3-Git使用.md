# Git



## Git干嘛的

Git是一个分布式版本控制系统，用于跟踪和管理代码的变化。它最初由Linux开发者Linus Torvalds在2005年创建，被广泛用于软件开发项目。

Git允许开发者在开发过程中记录和保存代码的各个版本，以便在需要时进行回溯、比较和恢复。它可以有效地处理多个开发者同时对同一个代码库进行修改的情况，帮助团队协作开发。

以下是Git的一些主要功能和特点：

1. 版本控制：Git可以跟踪文件的每个修改，并记录这些修改的详细历史。这使得开发者可以查看、比较和还原以前的版本。

2. 分支管理：Git支持创建和管理多个并行的分支，每个分支都可以独立开发和修改代码。这使得开发者可以在不影响主要代码的情况下进行实验、修复错误和添加新功能。

3. 协作与合并：多个开发者可以在不同的分支上独立工作，并通过合并（merge）将各自的修改合并到主要分支上。Git提供了强大的合并工具来自动处理合并冲突。

4. 远程仓库：Git可以与远程仓库进行交互，开发者可以将代码推送（push）到远程仓库，或者从远程仓库拉取（pull）最新的代码。这使得多个开发者可以协同工作，并方便地共享和备份代码。

5. 可靠性和性能：Git具有强大的错误检测和修复机制，能够有效地处理大型代码库。它使用了快照（snapshot）的方式来存储文件的状态，而不是基于差异（diff）的方式，提供了快速和高效的操作。

总之，Git是一个强大而灵活的工具，为开发者提供了一套有效的方式来管理和协作开发代码，促进团队合作，追踪代码变化，并确保代码的安全性和可靠性。

## Git的环境安装

要安装Git，您可以按照以下步骤进行操作：

1. 在您的操作系统上下载Git安装程序。您可以从Git官方网站（https://git-scm.com/downloads）上找到适用于您操作系统的安装程序。Git支持Windows、macOS和Linux等多种操作系统。

2. 运行安装程序并按照提示进行安装。在Windows上，双击下载的安装程序并按照向导进行操作。在macOS上，打开下载的安装程序并按照指示进行安装。在Linux上，您可以使用系统的软件包管理器来安装Git。

3. 在安装过程中，您可能需要进行一些配置。例如，您可以选择安装Git的组件和工具，设置Git在命令行中的默认文本编辑器等。根据您的需求进行相应的配置。

4. 安装完成后，打开命令行终端（例如，Windows上的命令提示符、macOS上的终端或Linux上的终端窗口）。

5. 输入以下命令验证Git是否成功安装并显示Git的版本信息：

```cmd
git --version
```

如果您能够看到Git的版本号，说明安装成功。

至此，您已经成功地安装了Git。现在您可以在命令行中使用Git命令来管理代码库，克隆远程仓库，创建新的分支，提交和合并代码等。

请注意，安装Git可能会因操作系统和版本而有所不同。以上步骤提供了一个一般的指南，但具体的步骤可能会有所变化。建议您查阅官方文档或针对您特定操作系统的安装指南以获取更详细的说明。

## Git简单使用

当您安装了Git之后，您可以通过以下简单的Git命令开始使用Git：

1. 创建一个新的Git仓库：
   ```
   git init
   ```
   这将在当前目录下创建一个新的Git仓库，开始进行版本控制。

2. 将文件添加到Git仓库：
   ```
   git add <文件名>
   ```
   使用这个命令将文件添加到Git仓库的暂存区。您可以指定单个文件的文件名，也可以使用通配符 `*` 来添加多个文件。

3. 提交文件到Git仓库：
   ```
   git commit -m "提交信息"
   ```
   使用这个命令将暂存区的文件提交到Git仓库，并附带一条简短的提交信息描述本次提交的内容。

4. 查看仓库状态：
   ```
   git status
   ```
   使用这个命令可以查看当前仓库的状态，包括已修改但未提交的文件、已暂存的文件等信息。

5. 查看提交历史：
   ```
   git log
   ```
   使用这个命令可以查看仓库的提交历史记录，包括每个提交的作者、时间、提交信息等。

6. 创建分支：
   ```
   git branch <分支名>
   ```
   使用这个命令可以创建一个新的分支，您可以在不影响主分支的情况下在新分支上进行修改和实验。

7. 切换分支：
   ```
   git checkout <分支名>
   ```
   使用这个命令可以切换到指定的分支，您可以在不同的分支之间进行切换和开发。

8. 合并分支：
   ```
   git merge <要合并的分支名>
   ```
   使用这个命令可以将指定的分支合并到当前所在的分支中，将其他分支的修改合并到当前分支。

9. 克隆远程仓库：
   ```
   git clone <远程仓库地址>
   ```
   使用这个命令可以克隆一个远程仓库到本地，获取远程仓库中的代码。

这些是Git的一些基本命令，可以帮助您进行基本的版本控制和代码管理。Git还有许多其他强大的功能，例如分支管理、远程仓库的操作等，您可以通过学习更多的Git命令和使用方法来深入了解和应用Git。

## Git的原理了解

Git的工作原理可以简单地描述为以下几个关键概念：

1. 代码仓库（Repository）：Git使用仓库来存储代码和历史记录。仓库可以是本地的，也可以是远程的。每个仓库都包含完整的代码历史和分支信息。

2. 快照（Snapshot）：Git以快照的方式存储代码的状态。每次提交（commit）时，Git会创建一个包含当前所有文件的快照，并记录这个快照的引用。

3. 分支（Branch）：分支是用来开发新功能、修复错误等目的的代码线路。每个分支在提交时都会创建一个新的快照。通过分支，多个开发者可以并行工作，而不会相互干扰。

4. 提交（Commit）：提交是将当前工作目录中的变化保存到Git仓库中的操作。每个提交都包含一个唯一的标识符（SHA-1哈希值）、作者、时间戳和提交消息等信息。

5. 暂存区（Staging Area）：暂存区是Git中的一个重要概念，它允许开发者选择性地将文件添加到下一次提交中。在执行提交之前，需要将修改的文件添加到暂存区。

6. 合并（Merge）：合并是将不同分支的修改合并到一起的操作。当多个分支的修改都准备好后，可以使用合并命令将这些修改合并到一个新的快照中。

7. 远程仓库（Remote Repository）：远程仓库是存放在网络上的Git仓库，通常用于多人协作和代码共享。开发者可以将本地的修改推送（push）到远程仓库，或者从远程仓库拉取（pull）最新的代码。

8. 差异（Diff）：Git使用差异算法来计算文件之间的差异，从而减少存储空间和传输数据的开销。当进行提交或合并时，Git会存储差异而不是完整的文件内容。

Git的核心原理是将代码的历史记录保存为一系列快照，并使用分支、提交和合并等概念来管理代码的变化。这种设计使得Git具有高效、可靠和灵活的版本控制能力，并且适用于各种规模的项目和团队。

## Git常见托管平台(gitee)使用



### 1、注册账号、设置邮箱

![img](https://img2022.cnblogs.com/blog/1448128/202207/1448128-20220705193452082-412064727.png)

 

新手最好**公开自己的邮箱**

 ![img](https://img2022.cnblogs.com/blog/1448128/202207/1448128-20220705193456090-504947824.png)

 

### 2、安装Git

https://git-scm.com/



### 3、配置RSA公钥

#### 1）在任一文件夹点鼠标右键，打开Git Bash

![img](https://img2022.cnblogs.com/blog/1448128/202207/1448128-20220705193911702-1427337613.png)

 

#### 2）输入以下指令，实现git账户与本地间的关联

```
ssh-keygen -t rsa -C "你的邮箱"
```

期间会出现3次输入，不用填写任何东西，直接三次回车即可。

#### 3）查看密钥

```
cat ~/.ssh/id_rsa.pub 
```

![img](https://img2022.cnblogs.com/blog/1448128/202207/1448128-20220705194333288-1212071096.png)

 

#### 4）将这一串密钥复制到Gitee中

说明：①复制可以先选中，再用鼠标右键copy，快捷键是CTRL+INSERT而非CTRL+C；

②复制到 个人→设置→SSH公钥

![img](https://img2022.cnblogs.com/blog/1448128/202207/1448128-20220705194633824-703288308.png)

 

 ![img](https://img2022.cnblogs.com/blog/1448128/202207/1448128-20220705194707210-483798622.png)

 

#### 5）测试是否连接到远程自己的账号

```
 ssh -T git@gitee.com
```

![img](https://img2022.cnblogs.com/blog/1448128/202207/1448128-20220705194836027-1096957135.png)

 

### 4、创建仓库

 1）打开官网，新建仓库

![img](https://img2022.cnblogs.com/blog/1448128/202207/1448128-20220705201615100-398572357.png)

 

 

2）建好仓库后，进入仓库中，点击**克隆/下载**，记录下**SSH地址**，之后会用到

 

**![img](https://img2022.cnblogs.com/blog/1448128/202207/1448128-20220705201855285-351989231.png)**

 

### 5、上传文件至仓库

#### 1）新建一个文件夹，名称任意，往其中随便放一两个文件

![img](https://img2022.cnblogs.com/blog/1448128/202207/1448128-20220705202019893-615817099.png)

 

2）在该文件夹中，点击鼠标右键，**Git Bash**：

![img](https://img2022.cnblogs.com/blog/1448128/202207/1448128-20220705202118842-827216489.png)

 

3）进行基础配置（也叫全局配置），其作用是告诉Git，之后谁将发布这些文件：

```
  git config --global user.name "你的名字或昵称"
  git config --global user.email "你的邮箱"
```

![img](https://img2022.cnblogs.com/blog/1448128/202207/1448128-20220705202312880-804710850.png)

 

#### 4）将该文件夹初始化为本地仓库

```
git init
```

之后该文件夹中会出现名为**.git**的文件夹

 ![img](https://img2022.cnblogs.com/blog/1448128/202207/1448128-20220705202447924-1073711304.png)

 

####  5）输入以下指令，将该文件夹链接到之前所说的SSH地址

下边的地址是在**第四节，2）步**所写的地址

```
git remote add origin 地址
```

![img](https://img2022.cnblogs.com/blog/1448128/202207/1448128-20220705202649527-1054144801.png)

 

#### 6）把本地仓库中的文件上传至gitee仓库

```
git add .
```

#### 7）添加注释，说明自己为什么要上传，方便以后自己查阅

```
 git commit -m "第一次上传" 
```

#### 8）提交

```
git push origin master
```

如果是第一次提交，则要写为：

```
git push -u origin master
```

第二次及以后，就不用加-u

注：如果最后一步报错，可以用以下指令强制覆盖

```
git push -f origin master
```

![img](https://img2022.cnblogs.com/blog/1448128/202207/1448128-20220705203152828-2055461153.png)

 

 

#### 9）刷新gitee的这个仓库，就可以看到上传的文件了

 

### 6、下载仓库到本地

随便新建个文件夹进入，右键打开**Git Bash**，输入如下指令：

```
git clone SSHurl
```

注：SSHurl是指想要下载的仓库的SSH地址，获得方式在第四节的第2）步。

### *7、基本命令汇总：

Git 的工作就是创建和保存你项目的快照及与之后的快照进行对比。他有四个位置：

- workspace：工作区
- staging area：暂存区/缓存区
- local repository：版本库或本地仓库
- remote repository：远程仓库

#### 常用指令

git init 初始化仓库
git clone 拷贝一份远程仓库，也就是下载一个项目。
git add 添加文件到暂存区
git status 查看仓库当前的状态，显示有变更的文件。
git diff 比较文件的不同，即暂存区和工作区的差异。
git commit 提交暂存区到本地仓库。
git reset 回退版本。
git rm 删除工作区文件。
git mv 移动或重命名工作区文件。
git log 查看历史提交记录
git blame <file> 以列表形式查看指定文件的历史修改记录
git remote 远程仓库操作
git fetch 从远程获取代码库
git pull 下载远程代码并合并
git push 上传远程代码并合并

#### 其他常见git命令

查看所有分支 ：git branch -a

切换到某一分支：git checkout 分支名称

合并分支：git merge 原分支 目标分支

#### 更新代码到本地

git status（查看本地分支文件信息，确保更新时不产生冲突）

git checkout -- [file name] （若文件有修改，可以还原到最初状态; 若文件需要更新到服务器上，应该先merge到服务器，再更新到本地）

git branch（查看当前分支情况）

git checkout remote branch

git pull

若命令执行成功，则更新代码成功！

可以直接使用： git pull 命令一步更新代码

## 个人仓库的部署

## 团队开发

### 首先管理员创建远程仓库

1. 管理员去码云上新建一个远程仓库
   - [gitee.com/](https://link.juejin.cn?target=https%3A%2F%2Fgitee.com%2F)

1. 克隆在码云上创建的远程仓库
   - git clone 远程仓库地址



![img](https://p1-jj.byteimg.com/tos-cn-i-t2oaga2asx/gold-user-assets/2020/6/2/172748d571898fc5~tplv-t2oaga2asx-zoom-in-crop-mark:4536:0:0:0.image)



1. **把项目放入你克隆下来的文件中**



![img](https://p1-jj.byteimg.com/tos-cn-i-t2oaga2asx/gold-user-assets/2020/6/2/172748f9d5edaa6e~tplv-t2oaga2asx-zoom-in-crop-mark:4536:0:0:0.image)



1. 提交时因为node_modelue太大我们选择不推送它
   - 用vim  .gitignore  （进去后按i输入node_modules然后保存），不推送node_modules



![img](https://p1-jj.byteimg.com/tos-cn-i-t2oaga2asx/gold-user-assets/2020/6/2/172748cfc2c16665~tplv-t2oaga2asx-zoom-in-crop-mark:4536:0:0:0.image)



1. 推送到暂存区
   - git add .



![img](https://p1-jj.byteimg.com/tos-cn-i-t2oaga2asx/gold-user-assets/2020/6/2/1727490de313bf30~tplv-t2oaga2asx-zoom-in-crop-mark:4536:0:0:0.image)



1. 由暂存区推送到本地仓库
   - git commit -m "本次推送的描述"



![img](https://p1-jj.byteimg.com/tos-cn-i-t2oaga2asx/gold-user-assets/2020/6/2/17274915c7f81b00~tplv-t2oaga2asx-zoom-in-crop-mark:4536:0:0:0.image)



1. 推送到远程仓库
   - git push



![img](https://p1-jj.byteimg.com/tos-cn-i-t2oaga2asx/gold-user-assets/2020/6/2/1727492cd8bca9cb~tplv-t2oaga2asx-zoom-in-crop-mark:4536:0:0:0.image)





![img](https://p1-jj.byteimg.com/tos-cn-i-t2oaga2asx/gold-user-assets/2020/6/2/17274944546c9bc8~tplv-t2oaga2asx-zoom-in-crop-mark:4536:0:0:0.image)



1. **添加开发这个项目的成员**



![img](https://p1-jj.byteimg.com/tos-cn-i-t2oaga2asx/gold-user-assets/2020/6/2/1727499e7acc5c9d~tplv-t2oaga2asx-zoom-in-crop-mark:4536:0:0:0.image)



> 小组成员在自己的码云上接受邀请



![img](https://p1-jj.byteimg.com/tos-cn-i-t2oaga2asx/gold-user-assets/2020/6/2/172749a9539d6fc6~tplv-t2oaga2asx-zoom-in-crop-mark:4536:0:0:0.image)



### 接下来就是小组成员的操作

1. **首先克隆项目**
2. **下载依赖**
3. **进行编辑**
4. **在git命令框中cd进入这个项目，查看一下当前的分支**



![img](https://p1-jj.byteimg.com/tos-cn-i-t2oaga2asx/gold-user-assets/2020/6/2/17274a08e5673c79~tplv-t2oaga2asx-zoom-in-crop-mark:4536:0:0:0.image)

5. **创建一个分支，创建好后在查看一下是否创建成功**





![img](https://p1-jj.byteimg.com/tos-cn-i-t2oaga2asx/gold-user-assets/2020/6/2/17274a11f9c12910~tplv-t2oaga2asx-zoom-in-crop-mark:4536:0:0:0.image)



1. **进入到这个分支**



![img](https://p1-jj.byteimg.com/tos-cn-i-t2oaga2asx/gold-user-assets/2020/6/2/17274a266511e71b~tplv-t2oaga2asx-zoom-in-crop-mark:4536:0:0:0.image)



1. **然后进行推送**



![img](https://p1-jj.byteimg.com/tos-cn-i-t2oaga2asx/gold-user-assets/2020/6/2/17274a37fbddaa10~tplv-t2oaga2asx-zoom-in-crop-mark:4536:0:0:0.image)



> 推送好后打开网页查看一下



![img](https://p1-jj.byteimg.com/tos-cn-i-t2oaga2asx/gold-user-assets/2020/6/2/17274a47d02db901~tplv-t2oaga2asx-zoom-in-crop-mark:4536:0:0:0.image)



### 写好后由管理员进行合并

1. 打开git 创建 分支 development
    git branch development
2. git checkout development 切换到 development 查看是否更新 git pull origin development   合并development  最新内容
3. 切换到主分支上 查看当前分支 git branch -v git checkout master
4. 合并分支 git merge development
5. git add .
6. git commit -m "描述"
7. git push origin master 推送到码云代码平台
    
