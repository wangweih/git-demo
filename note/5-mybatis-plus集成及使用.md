## ORM介绍

ORM 是 Object Relational Mapping 的缩写，译为“对象关系映射”框架。

所谓的 ORM 框架就是一种为了**解决面向对象与关系型数据库中数据类型不匹配的技术**，它通过描述 Java 对象与数据库表之间的映射关系，自动将 Java 应用程序中的对象持久化到关系型数据库的表中。

ORM 框架是一种数据持久化技术，即在对象模型和关系型数据库之间建立起对应关系，并且提供一种机制，可通过 JavaBean 对象操作数据库表中的数据

ORM框架本质就是简化编程中操作数据库的过程

## 在springboot中集成mybatis

### pom依赖导入

```pom
<!--        mysql连接-->

        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.33</version>
        </dependency>

<!--        数据库连接池-->
         <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>druid-spring-boot-starter</artifactId>
            <version>1.1.23</version>
        </dependency>
<!--        mybatis-->
        <dependency>
            <groupId>org.mybatis.spring.boot</groupId>
            <artifactId>mybatis-spring-boot-starter</artifactId>
            <version>2.2.0</version>
        </dependency>
```

### yaml配置

```yaml
  datasource:
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://localhost:3306/mysql_test?serverTimezone=Asia/Shanghai&characterEncoding=utf-8
    username: root
    password: root
    type: com.alibaba.druid.pool.DruidDataSource
```



### 添加扫包文件

```java
@SpringBootApplication
@MapperScan("自己的mapper的地址")
public class Application {
...
```

### 两种方式实现mapper

#### 使用注解的方式实现

| 注解            | 说明                                   |
| --------------- | -------------------------------------- |
| @Insert         | 实现新增                               |
| @Delete         | 实现删除                               |
| @Update         | 实现更新                               |
| @Select         | 实现查询                               |
| @Result         | 实现结果集封装                         |
| @Results        | 可以与@Result 一起使用，封装多个结果集 |
| @ResultMap      | 实现引用@Results 定义的封装            |
| @One            | 实现一对一结果集封装                   |
| @Many           | 实现一对多结果集封装                   |
| @SelectProvider | 实现动态 SQL 映射                      |

##### 实现方式

1. 创建mapper.XxxMapper.java文件
2. 修改为接口
3. 定义操作数据库方法
4. 在方法上打上对应注解及编写对应sql语句

```java
  	@Select("select * from teacher")
    public List<Teacher> findAllTea();

    @Select("select * from teacher where TId = #{tid}")
    public Teacher findTeaById(String tid);

    //直接传值可以使用#{}做占位符,填充参数值即可
    @Insert("insert into teacher set TId=#{tid},Tname=#{tname}")
    public int insertTeacher(String tid,String tname);

    
    //直接使用对象也可以,但注意对应属性名
    @Update("update teacher set Tname = #{tname} where TId = #{tId}")
    public int updateTeaById(Teacher teacher);
```

#### 使用xml进行操作

yaml配置

+ .xml文件应当创建在/resource路径下的文件夹中，并且需要在配置文件中指定位置

```yaml
    # 设置mybatis寻找对应.xml文件的位置
mybatis:
  mapper-locations: classpath:mapper/*Mapper.xml
  type-aliases-package: com.aistar.entity
```



- 构建*.xml文件时需要在命名空间中指定该文件对应的Mapper文件

  ```xml
  <mapper namespace="com.example.mybatis.mapper.MybatisTestMapper">
      ...
  </mapper>
  ```

- ```xml
  <!DOCTYPE mapper
      PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
      "https://mybatis.org/dtd/mybatis-3-mapper.dtd">
  
  <mapper namespace="org.apache.ibatis.submitted.rounding.Mapper">
    <resultMap type="org.apache.ibatis.submitted.rounding.User" id="usermap">
      <id column="id" property="id"/>
      <result column="name" property="name"/>
      <result column="funkyNumber" property="funkyNumber"/>
      <result column="roundingMode" property="roundingMode"/>
    </resultMap>
  
    <select id="getUser" resultMap="usermap">
      select * from users
    </select>
    <insert id="insert">
        insert into users (id, name, funkyNumber, roundingMode) values (
          #{id}, #{name}, #{funkyNumber}, #{roundingMode}
        )
    </insert>
  
    <resultMap type="org.apache.ibatis.submitted.rounding.User" id="usermap2">
      <id column="id" property="id"/>
      <result column="name" property="name"/>
      <result column="funkyNumber" property="funkyNumber"/>
      <result column="roundingMode" property="roundingMode" typeHandler="org.apache.ibatis.type.EnumTypeHandler"/>
    </resultMap>
    <select id="getUser2" resultMap="usermap2">
      select * from users2
    </select>
    <insert id="insert2">
        insert into users2 (id, name, funkyNumber, roundingMode) values (
          #{id}, #{name}, #{funkyNumber}, #{roundingMode, typeHandler=org.apache.ibatis.type.EnumTypeHandler}
        )
    </insert>
  
  </mapper>
  ```

- 



### 单元测试

```java
@RunWith(SpringRunner.class)
@SpringBootTest
...
```





## mybatis-plus的使用

### pom依赖

```xml
<!--        mybatis-plus  依赖于原来的 mybatis(包含)-->
        <dependency>
            <groupId>com.baomidou</groupId>
            <artifactId>mybatis-plus-boot-starter</artifactId>
            <version>3.5.3</version>
        </dependency>
        
```

### 全局配置yaml

```yaml
mybatis-plus:
  configuration:
    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl
```



### 使用方式

对应的mapper接口继承BaseMapper的类即可,<Student>里面修改为自己要操作的对象的映射

```java
public interface StudentMapper extends BaseMapper<Student>{
}
```



### 问题排除

```java
@MapperScan("com.aistar.mapper")
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class SpringBootDemoApplication {	
```



