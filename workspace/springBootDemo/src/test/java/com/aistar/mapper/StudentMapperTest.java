package com.aistar.mapper;

import com.aistar.entity.Student;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;


@SpringBootTest

public class StudentMapperTest {

    @Autowired
    StudentMapper studentMapper;

    @Test
    void studentMapperTest(){
        List<Student> students = studentMapper.selectList(null);

        for (Student student : students) {
            System.out.println(student);
        }
    }
}
