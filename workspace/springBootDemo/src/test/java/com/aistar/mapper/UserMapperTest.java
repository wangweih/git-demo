package com.aistar.mapper;

import com.aistar.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserMapperTest {

    @Autowired
    UserMapper userMapper;
    @Test
    void test(){
        User name = userMapper.findUserByname("name");
        System.out.println(name);
    }
}
