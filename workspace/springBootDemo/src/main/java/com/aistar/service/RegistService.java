package com.aistar.service;

import com.aistar.api.Result;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Service
public interface RegistService {

    //发送验证码,以及将code放到session中
    public Result sentCode(HttpSession session, String email);

}
