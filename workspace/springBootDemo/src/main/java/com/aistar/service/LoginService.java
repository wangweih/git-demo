package com.aistar.service;

import com.aistar.entity.User;

public interface LoginService {

    //对于登录的验证
    public String login(User user);

}
