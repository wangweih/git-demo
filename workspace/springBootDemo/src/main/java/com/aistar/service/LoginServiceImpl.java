package com.aistar.service;

import com.aistar.entity.User;
import com.aistar.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginServiceImpl implements LoginService{

    @Autowired
    private UserMapper userMapper;
    @Override
    public String login(User user) {

        //前端传来的用户数据
        String name = user.getName();
        String pwd = user.getPwd();

        System.out.println(name+":"+pwd);

        //获取数据库的用户数据
        User user1 = userMapper.findUserByname(name);

        if (null == user1){
            return "用户不存在";
        }

        if (!user1.getPwd().equals(pwd)){
            return "密码错误";
        }

        return "登录成功";

    }
}
