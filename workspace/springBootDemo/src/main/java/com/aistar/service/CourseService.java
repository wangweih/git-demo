package com.aistar.service;

import com.aistar.entity.Course;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CourseService {

    //查找所有的课程信息
    public List<Course> finfAllCouse();

    //删除课程信息
    public int deleteCouseById(String tId);
}
