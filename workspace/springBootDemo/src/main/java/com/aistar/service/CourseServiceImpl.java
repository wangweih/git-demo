package com.aistar.service;

import com.aistar.entity.Course;
import com.aistar.mapper.CourseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class CourseServiceImpl implements CourseService{

    @Autowired
    private CourseMapper courseMapper;
    @Override
    public List<Course> finfAllCouse() {
        return courseMapper.selectList(null);
    }

    @Override
    public int deleteCouseById(String tId) {

        int i = courseMapper.deleteById(tId);

            return i;

    }


}
