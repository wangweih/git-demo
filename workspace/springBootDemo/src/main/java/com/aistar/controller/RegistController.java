package com.aistar.controller;

import com.aistar.api.Result;
import com.aistar.entity.User;
import com.aistar.service.RegistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


@Controller
public class RegistController {

    @Autowired
    private RegistService registService;

    @GetMapping("/regist")
    public String init(){

        return "common/regist";

    }
    @PostMapping("/regist")
    @ResponseBody
    public Result regist(HttpServletRequest request,String code,User user){

        String rCode = (String) request.getSession().getAttribute("code");

        System.out.println(rCode+":"+code);

        if (!rCode.equals(code)){
            return Result.failed("验证码错误");
        }

        System.out.println(user);
        return Result.success();
    }
    @GetMapping("/send")
    @ResponseBody
    public Result send(String email,HttpServletRequest request){

        //创建session对象
        HttpSession session = request.getSession();
        //发送验证码,以及将我们的code放到session域中
        Result result = registService.sentCode(session, email);
        //返回结果
        return result;
    }
}
