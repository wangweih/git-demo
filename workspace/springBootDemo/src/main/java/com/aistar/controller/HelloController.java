package com.aistar.controller;

import com.aistar.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;


@Controller//返回静态资源
//@RestController//不可以 ResponseBody+Controller

public class HelloController {
    @RequestMapping("/hello")
    public String hello(HttpServletRequest request){
        User user = new User("aaa", "bbbb");

        request.setAttribute("user",user);


        return "welcome";

    }

    @RequestMapping("/getpage")
    public String getPage(){
        return "/img/test.html";

    }

    @RequestMapping("/helloi")
    public int helloi(){
        return 111;

    }
    @RequestMapping("/hello1")
    @ResponseBody
    public String hello1(){
        return "helloworld1";

    }
}
