package com.aistar.controller;

import com.aistar.entity.Course;
import com.aistar.mapper.CourseMapper;
import com.aistar.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class CourseController {

    @Autowired
    private CourseService courseService;

    @GetMapping("/course")
    public String init(HttpServletRequest request){


        //获取所有的列表
        List<Course> courseList = courseService.finfAllCouse();

        //将数据放到request作用域中,用于回显
        request.setAttribute("list",courseList);

        //重定向redirect:
        //转发
        return "index";
    }

    @GetMapping("/deletecourse")
    public String deleteCourse(@RequestParam(value = "cid") String cId,HttpServletRequest request){

        int i = courseService.deleteCouseById(cId);


        return "redirect:localhost:8080/course";
    }
}
