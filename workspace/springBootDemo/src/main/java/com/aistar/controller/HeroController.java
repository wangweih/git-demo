package com.aistar.controller;

import com.aistar.entity.Hero;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HeroController {

    @PostMapping("/attack")
    public Hero attack(@RequestBody Hero hero){

        new Hero(100,50).attack(hero);

        return hero;

    }
}
