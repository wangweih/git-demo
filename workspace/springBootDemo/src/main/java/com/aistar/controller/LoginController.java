package com.aistar.controller;

import com.aistar.entity.User;
import com.aistar.mapper.UserMapper;
import com.aistar.service.LoginService;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class LoginController {

    @Autowired
    private LoginService loginService;

    @Autowired
    UserMapper userMapper;
    //不要这么写,这么写只是为了演示



//    @RequestMapping(value = "/login",method = RequestMethod.POST)
    @PostMapping("/login")
    @ResponseBody
    public String name(User user){

//        String login = loginService.login(user);
//
//        System.out.println(login);

        return loginService.login(user);


    }
    @GetMapping("/login")
    public String getPage(){
        return "welcome";

    }




}
