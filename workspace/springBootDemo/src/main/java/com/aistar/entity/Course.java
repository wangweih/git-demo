package com.aistar.entity;


import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Course {

  @TableId
  private String cId;
  private String cName;
  private String tId;
  private String imgSrc;
  private String cInfo;


}
