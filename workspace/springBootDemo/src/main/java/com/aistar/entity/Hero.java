package com.aistar.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Hero {

    private Integer hp;
    private Integer acck;

    public Hero attack(Hero hero){
        hero.setHp(hero.getHp()-this.acck);
        return hero;
    }

}
