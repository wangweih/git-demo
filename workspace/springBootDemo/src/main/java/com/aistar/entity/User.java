package com.aistar.entity;

import lombok.*;

@Data
@AllArgsConstructor
public class User {
    private String name;
    private String pwd;
}
