package com.aistar.entity;

import com.baomidou.mybatisplus.annotation.TableId;

public class UserAccounts {
  @TableId
  private Integer id;
  private String password;
  private Integer resetPassword;
  private String mobile;
  private java.sql.Timestamp createAt;
  private java.sql.Timestamp updateAt;


  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }


  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }


  public Integer getResetPassword() {
    return resetPassword;
  }

  public void setResetPassword(Integer resetPassword) {
    this.resetPassword = resetPassword;
  }


  public String getMobile() {
    return mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }


  public java.sql.Timestamp getCreateAt() {
    return createAt;
  }

  public void setCreateAt(java.sql.Timestamp createAt) {
    this.createAt = createAt;
  }


  public java.sql.Timestamp getUpdateAt() {
    return updateAt;
  }

  public void setUpdateAt(java.sql.Timestamp updateAt) {
    this.updateAt = updateAt;
  }

}
