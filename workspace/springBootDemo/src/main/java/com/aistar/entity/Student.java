package com.aistar.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

public class Student {
  @TableId
  private String sId;//---- s_id
  private String sname;// sname
  private java.sql.Timestamp sage;
  private String ssex;
  //注解
  @TableField(exist = false)
  private Teacher teacher;




  public String getSId() {
    return sId;
  }

  public void setSId(String sId) {
    this.sId = sId;
  }


  public String getSname() {
    return sname;
  }

  public void setSname(String sname) {
    this.sname = sname;
  }


  public java.sql.Timestamp getSage() {
    return sage;
  }

  public void setSage(java.sql.Timestamp sage) {
    this.sage = sage;
  }


  public String getSsex() {
    return ssex;
  }

  public void setSsex(String ssex) {
    this.ssex = ssex;
  }

}
