package com.aistar.mapper;

import com.aistar.entity.Teacher;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface TeacherMapper {

    @Select("select * from teacher")
    public List<Teacher> findAllTea();

    @Select("select * from teacher where TId = #{tid}")
    public Teacher findTeaById(String tid);

    //直接传值可以使用#{}做占位符,填充参数值即可
    @Insert("insert into teacher set TId=#{tid},Tname=#{tname}")
    public int insertTeacher(String tid,String tname);


    //直接使用对象也可以,但注意对应属性名
    @Update("update teacher set Tname = #{tname} where TId = #{tId}")
    public int updateTeaById(Teacher teacher);



}
