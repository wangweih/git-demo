package com.aistar.mapper;

import com.aistar.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserMapper  {

    @Select("select u.name,u.pwd from user u where u.name = #{name}")
    public User findUserByname(String name);

    @Delete("delete from user where name = #{name}")
    public int deleteUserByName(String name);
}
