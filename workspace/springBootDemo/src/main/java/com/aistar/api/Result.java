package com.aistar.api;

import lombok.Data;

import java.io.Serializable;
@Data
public class Result<T> implements Serializable {
// 自定义状态码
private Integer code;
// 提示内容，如果接口出错，则存放异常信息
private String msg;
// 返回数据体
private T data;
// 接口成功检测。拓展字段，前台可用该接口判断接口是否正常，或者通过code状态码
private static final long serialVersionUID = 1L;

public Result() {}

public Result(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        }

/**
 * 请求成功返回
 * public和返回值间的<T>指定的这是一个泛型方法，这样才可以在方法内使用T类型的变量
 * @author
 * @date
 */
public static <T> Result<T> success() {

        return new Result<>(200, "success", null);
        }
 public static <T> Result<T> success(String msg) {
                return new Result<>(200, msg, null);
        }
public static <T> Result<T> success(T data) {
        return new Result<>(200, "success", data);
        }

/**
 * 请求失败返回
 * @param msg:
 * @author
 * @date
 */
public static <T> Result<T> failed(String msg) {
        return new Result<>(500, msg, null);
        }

public static <T> Result<T> failed(String msg, T data) {
        return new Result<>(500, msg, data);
        }


@Override
public String toString() {
        return "Result{" + "code=" + code +
        ", msg='" + msg + '\'' +
        ", data=" + data +
        '}';
        }
        }

