<%--
  Created by IntelliJ IDEA.
  User: Dean
  Date: 2023-07-03
  Time: 0:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>注册</title>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css">
  <style>
    .container {
      max-width: 400px;
      margin: 0 auto;
      margin-top: 100px;
    }

    h1 {
      text-align: center;
      margin-bottom: 40px;
    }
  </style>

</head>
<body>
<div class="container">
  <h1>邮箱注册</h1>
  <form id="register-form">
    <div class="mb-3">
      <label for="email" class="form-label">邮箱地址</label>
      <input type="email" class="form-control" id="email" placeholder="请输入邮箱地址">
    </div>
    <div class="mb-3">
      <label for="verification-code" class="form-label">验证码</label>
      <div class="input-group">
        <input type="text" class="form-control" id="verification-code" placeholder="请输入验证码">
        <button type="button" class="btn btn-outline-primary" id="send-verification-code" onclick="send_email()">发送验证码</button>
      </div>
    </div>
    <div class="mb-3">
      <label for="password" class="form-label">密码</label>
      <input type="password" class="form-control" id="password" placeholder="请输入密码">
    </div>
    <div class="mb-3">
      <label for="confirm-password" class="form-label">确认密码</label>
      <input type="password" class="form-control" id="confirm-password" placeholder="请确认密码">
    </div>
<%--    <div class="form-check mb-3">--%>
<%--      <input type="checkbox" class="form-check-input" id="remember-me">--%>
<%--      <label class="form-check-label" for="remember-me">记住我</label>--%>
<%--    </div>--%>
    <button type="submit" class="btn btn-primary" onclick="regist()">注册</button>
  </form>
  <p class="mt-3 text-center">已有账号？<a href="login.html">点击登录</a></p>
</div>
<script src="resource/js/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
<script>

  function send_email(){

    //获取在前端的email
    let email = $("#email").val();

    // let pwd1 = $("password").val();
    // let pwd2 = $("confirm-password").val();
    //
    // if(pwd1!=pwd2){
    //   alert("密码不一致")
    // }

    $.get("/send",{email:email},function(result){
      console.log("okok")
      if(result.code == '200'){
        alert("验证码发送成功");
      }else{
        alert("发送失败,请重新尝试");
      }
    });
  }
  function regist(){
    let code = $("#verification-code").val();
    let username = $("#email").val();
    let password=$("#password").val();

    $.post("/regist",{code:code,name:username,pwd:password},function (result) {
      if (result.code==200){
        alert("注册成功,请登录")
        setTimeout(function() {
          window.location.href = "/login"; // 替换为您要跳转的 URL
        }, 1000);
      }
      else {
        alert(result.msg);
      }
    })

  }

</script>

</body>
</html>
