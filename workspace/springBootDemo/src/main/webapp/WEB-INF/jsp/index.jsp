<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
          <a class="navbar-brand" href="#">Navbar</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="#">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Link</a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Dropdown
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li><a class="dropdown-item" href="#">Action</a></li>
                  <li><a class="dropdown-item" href="#">Another action</a></li>
                  <li><hr class="dropdown-divider"></li>
                  <li><a class="dropdown-item" href="#">Something else here</a></li>
                </ul>
              </li>
              <li class="nav-item">
                <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
              </li>
            </ul>
            <form class="d-flex">
              <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
              <button class="btn btn-outline-success" type="submit">Search</button>
            </form>
          </div>
        </div>
      </nav>

    <div><a href="/addcourse/${item.CId}" class="btn btn-primary col-sm">增加</a></div>
    <div class="container-fluid">



      <c:forEach items="${requestScope.courselist}" var="item">
      <div class="card" style="width: 18rem;">
        <img class="card-img-top" src="resource/img/${item.imgSrc}" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">${item.CName}</h5>
          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          <a href="#" class="btn btn-primary">Go somewhere</a>
        </div>
      </div>
      </c:forEach>


      <div class="row">
      <c:forEach items="${requestScope.list}" var="item">

        <div class="card col-sm" style="width: 18rem;" id="${item.CName}">
            <img src="/resource/img/${item.imgSrc}" class="card-img-top" alt="..." style="width: ">
            <div class="card-body">
              <h5 class="card-title">${item.CName}</h5>
              <p class="card-text">${item.CInfo}</p>
              <div class="row">
                <a href="/selectcourse/${item.CId}" class="btn btn-primary col-sm">查看</a>
                <a href="/updatecourse/${item.CId}" class="btn btn-primary col-sm">修改</a>
                <a href="/deletecourse?cid=${item.CId}" onclick="deleteDiv('${item.CName}')" class="btn btn-primary col-sm">删除</a>
              </div>

            </div>
          </div>
      </c:forEach>
      </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/mhDoLbDldZc3qpsJHpLogda//BVZbgYuw6kof4u2FrCedxOtgRZDTHgHUhOCVim" crossorigin="anonymous"></script>
  </body>

  <script>
    function deleteDiv(id) {
      var div = document.getElementById(id);
      div.parentNode.removeChild(div);
    }
  </script>
</html>