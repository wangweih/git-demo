package oop;

public class User {
    private String name;
    private String pwd;

    /*
    函数或者叫方法 调用函数:函数名([参数]);

    标准函数的格式
    修饰词(public) 返回类型 函数名(参数){

    retrun
    }

    /无参的构造方法
     */

    public User()
{
        System.out.println("无参构造被调用了");
    }

    //无参的构造方法
    public User(String name1,String pwd){
        this.name = name1;
        this.pwd = pwd;

        System.out.println("有参被调用了"+name+":"+pwd);
    }

    public String getName(){

        System.out.println("getName被调用了");
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", pwd='" + pwd + '\'' +
                '}';
    }




}
