package oop;

public class Hero {
//    血量
    private int hp;
    private int ack;

    public Hero() {

        System.out.println("hero 被调用了");
    }

    public Hero(int hp, int ack) {
        this.hp = hp;
        this.ack = ack;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getAck() {
        return ack;
    }

    public void setAck(int ack) {
        this.ack = ack;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Hero{");
        sb.append("hp=").append(hp);
        sb.append(", ack=").append(ack);
        sb.append('}');
        return sb.toString();
    }

    //业务函数
    public void attack(Hero hero){
        int hp1 = hero.getHp()-this.ack;


        if (hp1<=0){
            System.out.println("这小子死了");
            return;
        }



        hero.setHp(hp1);

        System.out.println(hero.getHp());

    }
}
